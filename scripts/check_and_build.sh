#!/bin/bash

# ./version_check.sh <project> <image> <branch>

if [[ -z "$1" ]]; then
    echo "$0 <project> <image> <v11|v12>"
    exit 0
fi

docker manifest inspect $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$1/$2:$VERSION > /dev/null \
    && export IMAGE_EXISTS=1 || echo "Proceed to build image"

if [ "$IMAGE_EXISTS" = 1 ]; then
    echo "Image already exists"
    exit 0
else

    docker build -t $2:$VERSION $2 -f $2/$3.Dockerfile
    docker tag $2:$VERSION $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$1/$2:$VERSION
    docker push $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$1/$2:$VERSION

fi
