# Decoupled Docker images Frappe / ERPNext

ERPNext images are Python 3 only. For SocketIO NodeJS used is latest stable.
For built assets NodeJS is tagged to v10 for v11 and v12 for v12.

### ERPNext Assets

nginx image to serve assets and reverse proxy to services
On container entry it copies asset files to /assets directory.
These assets are required by python image.

### EPRNext Python

- This image has erpnext and frappe installed as python app
- If `RUN_AS_ROOT=1` all processes are run as root
- Serves the frappe/erpnext app via gunicorn
- Runs workers with `command: worker`,
- For worker set `WORKER_TYPE` as `default`, `long` or `short`, e.g `WORKER_TYPE=default`
- command: `docker-entrypoint.sh new` creates new site as per ENV variables
- command: `docker-entrypoint.sh migrate` migrates site(s) as per ENV variable
- command: `docker-entrypoint.sh doctor` checks bench status
- command: `docker-entrypoint.sh backup` backs up site(s) as per ENV variable
- command: `docker-entrypoint.sh schedule` starts scheduler

### Frappe SocketIO

This image is used to serve frappe socketio
`health.js` is added for container health checks

# Getting Started

Clone the repo and change working directory

```sh
git clone https://gitlab.com/castlecraft/erpnext_kubernetes.git && cd erpnext_kubernetes
```

Copy `env-example` to `.env`

```sh
cp env-example .env
```

Start services.

```sh
# Images from develop branch
docker-compose up

# Images from version-11 branch
docker-compose \
    --project-name erpnext_kubernetes \
    -f docker/docker-compose-v11.yml \
    --project-directory . up

# Images from version-12 branch
docker-compose \
    --project-name erpnext_kubernetes \
    -f docker/docker-compose-v12.yml \
    --project-directory . up
```

Notes:

- After one of the above `docker-compse ... up` commands is executed the terminal remains open and you will proceed in a separate terminal.
- There are multiple docker-compose.yml available with suffix. e.g `v11` and `v12`
- Main docker-compose.yml is based on edge images, which are images built from develop branch.
- Wait for mariadb to start successfully
- For multiple benches, update docker-compose for nginx ports. default exposed port is 80

## Create new site

- `INSTALL_ERPNEXT` is optional. By default script will only add a frappe site.
- `FORCE` is optional. If set it will force reinstall new-site. By default script will not force reinstall new site

```sh
docker exec -it \
    -e SITE_NAME=test.localhost \
    -e DB_ROOT_USER=root \
    -e DB_ROOT_PASSWORD=admin \
    -e ADMIN_PASSWORD=admin \
    -e INSTALL_ERPNEXT=1 \
    erpnext_kubernetes_erpnext-python_1 docker-entrypoint.sh new
```

Visit url site url (http://test.localhost) from browser to complete setup wizard

## Backup sites(s)

`SITES` env variable is list of sites separated by `:` to migrate. e.g. `SITES=site1.localhost` or `SITES=site1.localhost:site2.localhost`

By default all sites in bench will be backed up

`WITH_FILES` env variable backs up user uploaded files for the sites

```sh
docker exec -it \
    -e "SITES=site1.localhost:site2.localhost" \
    -e "WITH_FILES=1" \
    erpnext_kubernetes_erpnext-python_1 docker-entrypoint.sh backup
```

## Migrate site(s)

`SITES` env variable is list of sites separated by `:` to migrate. e.g. `SITES=site1.localhost` or `SITES=site1.localhost:site2.localhost`.

By default all sites in bench will migrate

`MAINTENANCE_MODE` env variable pauses the scheduler and sets bench to maintainence mode before migration

```sh
docker exec -it \
    -e "SITES=site1.localhost:site2.localhost" \
    -e "MAINTENANCE_MODE=1" \
    erpnext_kubernetes_erpnext-python_1 docker-entrypoint.sh migrate
```
